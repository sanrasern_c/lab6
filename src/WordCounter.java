import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

/**
 * To count how many times different words occur.
 * @author Sanrasern Chaihetphon 5710547247	
 *
 */
public class WordCounter {
	/**
	 * Create a Map to collect the data.
	 */
	Map<String ,Integer> map = new HashMap<String, Integer>();
	
	/**
	 * Add word in to a Map.
	 * @param word is word to collect in to Map.
	 */
	public void addWorld(String word){
		String lowerWord = word.toLowerCase();
		if(map.containsKey(lowerWord)){
			int count = map.get(lowerWord) + 1;
			map.replace(lowerWord, count);
			
		}
		else{
			map.put(lowerWord, 1);
		}
	}
	
	/**
	 * Turn words in Map to Set.
	 * @return Set<String> of the words.
	 */
	public Set<String> getWords(){
		return (Set<String>) map.keySet();
	}
	
	/**
	 * Give the number of word that duplicate.
	 * @param word is word to know the how many the duplicate.
	 * @return number of that word duplicate.
	 */
	public int getCount(String word){
		return map.get(word);
	}
	
	/**
	 * To order word by alphabet and collect in array.
	 * @return array of alphabetical order.
	 */
	public String [] getSortedWords(){
		 List orderWord = new ArrayList(map.keySet());
		 Collections.sort(orderWord);
		 String[] temp = new String[orderWord.size()];
		 orderWord.toArray(temp);
		 return temp;
	}
	
}
