import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * To test the WordCounter.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public class WordCount {
	public static void main(String [] args) throws IOException {
		WordCounter counter = new WordCounter();
		String FILE_URL = "https://gist.githubusercontent.com/anonymous/0f0374e431bb88f403c9/raw/b8ad310a887093bca6bcc54192677c24bb00a7a2/AliceInWonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(DELIMS);
		
		while(scanner.hasNext()){
			
			counter.addWorld(scanner.next());
		}
		for(int i=0; i<20; i++){
			System.out.println(counter.getSortedWords()[i] + " : " + counter.getCount(counter.getSortedWords()[i]));
		}
		
		
	}
}
